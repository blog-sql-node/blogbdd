//----------------------------------------------> IMPORT DE BIBLIOTHÈQUE 
const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const mustache = require('mustache');
const mustache_express = require('mustache-express');


const app = express();
// ----------------------------------------------> MIDDLEWARE ENVIRONNEMENT ( mmustache & bodyparser & dossier public)
app.engine('mustache', mustache_express());
app.set('view engine', 'mustache');// pour reconnaitre les fichier mustache 
app.set('views',__dirname + '/view'); //--- accès au dossier view
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));

// ----------------------------------------------> CONNEXION AVEC BDD

const base_de_donnees = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'root',
	database: 'Blog',
});

base_de_donnees.connect(function (err) {
	if (err) {
		throw err;
	} else {
		console.log('connection avec bdd opérationnelle')
	}
});

app.get('/', function (req, res) {
	res.sendFile(__dirname +'/public/Register.html');
});

//-------------------------------------------> ROUTE INSCRIPTION : Recuperer les valeur de la page Inscription 
app.post('/', function (req, res) {
	let user_name = req.body.user_name;
	// console.log("Nom : " + user_name );
	let user_prenom = req.body.user_prenom;
	// console.log("Prenom : " + user_prenom );
	let user_login = req.body.user_login;
	// console.log("Pseudo: " + user_login );
	let user_email = req.body.user_email;
	// console.log("Email : " + user_email );
	let user_mdp = req.body.user_mdp;
	// console.log("Password : " + user_mdp );
	// -------------------------------------> Envoie des données sur la BDD
	base_de_donnees.query(`INSERT INTO User(nom,prenom,pseudo,mail,mdp) VALUES("${user_name}","${user_prenom}","${user_login}","${user_email}","${user_mdp}")`)
	console.log("Envoie données sur la BDD")
	//res.send( "Nom : " + user_name + "Prenom : " + user_prenom + "Pseudo: " + user_login + "Email : " + user_email +" "+ "Password : " + user_mdp );
	res.redirect("/articles");
});

//-------------------------------------------> ROUTE CONNEXION :  Recuperer les valeur de la page Connexion 
app.post('/connexion', function (req, res) {
	let user_email = req.body.user_email;
	let user_mdp = req.body.user_mdp;
	//console.log(  "Email : " + req.body.user_email );
	//console.log("Password : " +req.body.user_mdp );
	//res.send( "Email : " + user_email +" "+ "Password : " + user_mdp);
	res.redirect("/articles");

});
//-------------------------------------------> ROUTE POSTS : Faire apparaitre les poste dans le html
// app.post('/index', function (req, res) {
// 	let titre = req.body.titre;
// 	console.log("Titre : " + titre);
// 	let article = req.body.article;
// 	console.log("Article : " + article);
// 	//base_de_donnees.query(`INSERT INTO Articles(titre,contenu) VALUES("${titre}","${article}")`)
// 	//res.send("Titre : " + titre + "Article : " + article);
// 	res.redirect("/index.html");
// })
// ----------------------------------------------> LISTER LES POSTS A L'ARRIVER SUR LA PAGE
app.get('/articles', function(req,res){

	base_de_donnees.query(`SELECT titre, contenu FROM Articles `,function (error, articles, fields){
		if(error){
			console.log(error);
		}
		else{
			res.render("article",{articles});
		}
		
	});

})
// ----------------------------------------------> SAUVEGARDER LES NOUVEAUX POSTS( envoie BDD)
app.post('/articles/ajout', function(req,res){
	let titre = req.body.titre;
	console.log(titre);
	let article = req.body.article;
	console.log(article);

	base_de_donnees.query(`INSERT INTO Articles (titre,contenu,User_id) VALUES("${titre}","${article}",${5})`, function(error,article, fields){
		if(error){
			console.log(error);
		}
		else{
			res.redirect('/articles');
		}
	});

//--------------------------------------------------------> lister tous les postes de l'utilisateur X à son arrivée sur la page
});
app.get('/mes_articles/:id', function(req,res){

	console.log(req.params.id);

	base_de_donnees.query(`SELECT titre FROM Articles WHERE user_id = ? `,[req.params.id],function (error, articles_perso, fields){
		if(error){
			console.log(error);
		}
		else{
			console.log(articles_perso);
			res.render("mes_articles",{"articles_perso": articles_perso});
		}
		
	});

})




// ----------------------------------------------> LANCEMENT SERVEUR 

app.listen(3681, function () {
	console.log("Serveur lancé")
});