var mysql = require('mysql');


var mySqlClient = mysql.createConnection({
  host     : "localhost",
  user     : "user",
  password : "password",
  database : "user"
});

var selectQuery = 'SELECT * FROM user';

mySqlClient.query(selectQuery,function select(error, results, fields) {
    if (error) {
      console.log(error);
      mySqlClient.end();
      return;
    }
      
    if ( results.length > 0 )  { 
      var firstResult = results[ 0 ];
      console.log('id: ' + firstResult['id']);
      console.log('label: ' + firstResult['label']);
      console.log('valeur: ' + firstResult['valeur']);
    } else {
      console.log("Pas de données");
    }
    mySqlClient.end();
  }
);